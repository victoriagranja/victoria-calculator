const $ = document;

let total = 0;

function adicionar(num){
    let pai = $.querySelector("#list");
    let filho = $.createElement("div");
    filho.innerText = num;
    filho.classList.add("bg-blue");
    pai.appendChild(filho);
}

let elemButCalc = $.querySelector(".bt-calc");

let elemButAdd = $.querySelector("#bt-soma");
elemButAdd.addEventListener("click", () => {
    let num1 = parseInt($.querySelector("#val1").value);
    let num2 = parseInt($.querySelector("#val2").value);
    total = num1 + num2;
});


let elemButSubt = $.querySelector("#bt-subt");
elemButSubt.addEventListener("click", () => {
    let num1 = parseInt($.querySelector("#val1").value);
    let num2 = parseInt($.querySelector("#val2").value);
    total = num1 - num2;
});

let elemButDivis = $.querySelector("#bt-divis");
elemButDivis.addEventListener("click", () => {
    let num1 = parseInt($.querySelector("#val1").value);
    let num2 = parseInt($.querySelector("#val2").value);
    total = num1 / num2;
});

let elemButMulti = $.querySelector("#bt-multi");
elemButMulti.addEventListener("click", () => {
    let num1 = parseInt($.querySelector("#val1").value);
    let num2 = parseInt($.querySelector("#val2").value);
    total = num1 * num2;
});

elemButCalc.addEventListener("click", () => {
    adicionar(total);
});

let elem = $.querySelector("#list");

let remover = function(e){
    elem.removeChild(e.target);
}

elem.addEventListener("click", remover);

let rem = $.querySelector(".bt-reset");
rem.addEventListener("click", () => {
    for(let i = elem.children.length; i > 0; i--) {
        elem.children[i-1].remove();   
    }    
});